/**
 * GENERATED CODE - DO NOT CHANGE
 */

import astra.core.*;
import astra.execution.*;
import astra.event.*;
import astra.messaging.*;
import astra.formula.*;
import astra.lang.*;
import astra.statement.*;
import astra.term.*;
import astra.type.*;
import astra.tr.*;
import astra.reasoner.util.*;


public class Main extends ASTRAClass {
	public Main() {
		setParents(new Class[] {astra.lang.Agent.class});
		addRule(new Rule(
			"Main", new int[] {6,13,6,32},
			new GoalEvent('+',
				new Goal(
					new Predicate("main", new Term[] {
						new Variable(Type.LIST, "args",false)
					})
				)
			),
			Predicate.TRUE,
			new Block(
				"Main", new int[] {6,31,13,9},
				new Statement[] {
					new Declaration(
						new Variable(Type.INTEGER, "X"),
						"Main", new int[] {7,13,13,9},
						Primitive.newPrimitive(0)
					),
					new While(
						"Main", new int[] {8,13,13,9},
						new Comparison("<",
							new Variable(Type.INTEGER, "X"),
							Primitive.newPrimitive(5)
						),
						new Block(
							"Main", new int[] {8,26,11,14},
							new Statement[] {
								new ModuleCall("system",
									"Main", new int[] {9,19,9,62},
									new Predicate("createAgent", new Term[] {
										Operator.newOperator('+',
											Primitive.newPrimitive("hello"),
											new Variable(Type.INTEGER, "X")
										),
										Primitive.newPrimitive("HelloWorld")
									}),
									new DefaultModuleCallAdaptor() {
										public boolean inline() {
											return true;
										}

										public boolean invoke(Intention intention, Predicate predicate) {
											return ((astra.lang.System) intention.getModule("Main","system")).createAgent(
												(java.lang.String) intention.evaluate(predicate.getTerm(0)),
												(java.lang.String) intention.evaluate(predicate.getTerm(1))
											);
										}
									}
								),
								new Assignment(
									new Variable(Type.INTEGER, "X"),
									"Main", new int[] {10,19,11,14},
									Operator.newOperator('+',
										new Variable(Type.INTEGER, "X"),
										Primitive.newPrimitive(1)
									)
								)
							}
						)
					),
					new Subgoal(
						"Main", new int[] {12,13,13,9},
						new Goal(
							new Predicate("awaitChildTermination", new Term[] {})
						)
					)
				}
			)
		));
		addRule(new Rule(
			"Main", new int[] {16,13,16,39},
			new GoalEvent('+',
				new Goal(
					new Predicate("awaitChildTermination", new Term[] {})
				)
			),
			Predicate.TRUE,
			new Block(
				"Main", new int[] {16,38,25,9},
				new Statement[] {
					new Declaration(
						new Variable(Type.INTEGER, "N"),
						"Main", new int[] {17,13,25,9},
						new ModuleTerm("prelude", Type.INTEGER,
							new Predicate("size", new Term[] {
								new ModuleTerm("system", Type.LIST,
									new Predicate("getAgents", new Term[] {}),
									new ModuleTermAdaptor() {
										public Object invoke(Intention intention, Predicate predicate) {
											return ((astra.lang.System) intention.getModule("Main","system")).getAgents(
											);
										}
										public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
											return ((astra.lang.System) visitor.agent().getModule("Main","system")).getAgents(
											);
										}
									}
								)
							}),
							new ModuleTermAdaptor() {
								public Object invoke(Intention intention, Predicate predicate) {
									return ((astra.lang.Prelude) intention.getModule("Main","prelude")).size(
										(astra.term.ListTerm) intention.evaluate(predicate.getTerm(0))
									);
								}
								public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
									return ((astra.lang.Prelude) visitor.agent().getModule("Main","prelude")).size(
										(astra.term.ListTerm) visitor.evaluate(predicate.getTerm(0))
									);
								}
							}
						)
					),
					new While(
						"Main", new int[] {18,18,25,9},
						new Comparison(">",
							new Variable(Type.INTEGER, "N"),
							Primitive.newPrimitive(1)
						),
						new Block(
							"Main", new int[] {18,31,22,19},
							new Statement[] {
								new ModuleCall("console",
									"Main", new int[] {19,22,19,69},
									new Predicate("println", new Term[] {
										Operator.newOperator('+',
											Primitive.newPrimitive("there are: "),
											Operator.newOperator('+',
												new Variable(Type.INTEGER, "N"),
												Primitive.newPrimitive(" agents.")
											)
										)
									}),
									new DefaultModuleCallAdaptor() {
										public boolean inline() {
											return true;
										}

										public boolean invoke(Intention intention, Predicate predicate) {
											return ((astra.lang.Console) intention.getModule("Main","console")).println(
												(java.lang.String) intention.evaluate(predicate.getTerm(0))
											);
										}
									}
								),
								new ModuleCall("system",
									"Main", new int[] {20,22,20,39},
									new Predicate("sleep", new Term[] {
										Primitive.newPrimitive(1000)
									}),
									new DefaultModuleCallAdaptor() {
										public boolean inline() {
											return false;
										}

										public boolean invoke(Intention intention, Predicate predicate) {
											return ((astra.lang.System) intention.getModule("Main","system")).sleep(
												(java.lang.Integer) intention.evaluate(predicate.getTerm(0))
											);
										}
									}
								),
								new Assignment(
									new Variable(Type.INTEGER, "N"),
									"Main", new int[] {21,22,22,19},
									new ModuleTerm("prelude", Type.INTEGER,
										new Predicate("size", new Term[] {
											new ModuleTerm("system", Type.LIST,
												new Predicate("getAgents", new Term[] {}),
												new ModuleTermAdaptor() {
													public Object invoke(Intention intention, Predicate predicate) {
														return ((astra.lang.System) intention.getModule("Main","system")).getAgents(
														);
													}
													public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
														return ((astra.lang.System) visitor.agent().getModule("Main","system")).getAgents(
														);
													}
												}
											)
										}),
										new ModuleTermAdaptor() {
											public Object invoke(Intention intention, Predicate predicate) {
												return ((astra.lang.Prelude) intention.getModule("Main","prelude")).size(
													(astra.term.ListTerm) intention.evaluate(predicate.getTerm(0))
												);
											}
											public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
												return ((astra.lang.Prelude) visitor.agent().getModule("Main","prelude")).size(
													(astra.term.ListTerm) visitor.evaluate(predicate.getTerm(0))
												);
											}
										}
									)
								)
							}
						)
					),
					new ModuleCall("console",
						"Main", new int[] {23,18,23,50},
						new Predicate("println", new Term[] {
							Primitive.newPrimitive("Finished Task")
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Console) intention.getModule("Main","console")).println(
									(java.lang.String) intention.evaluate(predicate.getTerm(0))
								);
							}
						}
					),
					new ModuleCall("system",
						"Main", new int[] {24,18,24,31},
						new Predicate("exit", new Term[] {}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.System) intention.getModule("Main","system")).exit(
								);
							}
						}
					)
				}
			)
		));
	}

	public void initialize(astra.core.Agent agent) {
	}

	public Fragment createFragment(astra.core.Agent agent) throws ASTRAClassNotFoundException {
		Fragment fragment = new Fragment(this);
		fragment.addModule("system",astra.lang.System.class,agent);
		fragment.addModule("prelude",astra.lang.Prelude.class,agent);
		fragment.addModule("console",astra.lang.Console.class,agent);
		return fragment;
	}

	public static void main(String[] args) {
		Scheduler.setStrategy(new AdaptiveSchedulerStrategy());
		ListTerm argList = new ListTerm();
		for (String arg: args) {
			argList.add(Primitive.newPrimitive(arg));
		}

		String name = java.lang.System.getProperty("astra.name", "main");
		try {
			astra.core.Agent agent = new Main().newInstance(name);
			agent.initialize(new Goal(new Predicate("main", new Term[] { argList })));
			Scheduler.schedule(agent);
		} catch (AgentCreationException e) {
			e.printStackTrace();
		} catch (ASTRAClassNotFoundException e) {
			e.printStackTrace();
		};
	}
}
