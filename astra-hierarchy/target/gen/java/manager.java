/**
 * GENERATED CODE - DO NOT CHANGE
 */

import astra.core.*;
import astra.execution.*;
import astra.event.*;
import astra.messaging.*;
import astra.formula.*;
import astra.lang.*;
import astra.statement.*;
import astra.term.*;
import astra.type.*;
import astra.tr.*;
import astra.reasoner.util.*;


public class manager extends ASTRAClass {
	public manager() {
		setParents(new Class[] {Shared.class});
		addRule(new Rule(
			"manager", new int[] {22,9,22,105},
			new GoalEvent('+',
				new Goal(
					new Predicate("decoded", new Term[] {
						new Variable(Type.INTEGER, "Id",false)
					})
				)
			),
			new AND(
				new NOT(
					new Predicate("decoded", new Term[] {
						new Variable(Type.INTEGER, "Id")
					})
				),
				new AND(
					new Predicate("generator", new Term[] {
						new Variable(Type.STRING, "Gen",false)
					}),
					new Predicate("encodedString", new Term[] {
						new Variable(Type.INTEGER, "Id"),
						new Variable(Type.LIST, "Codes",false)
					})
				)
			),
			new Block(
				"manager", new int[] {22,104,25,5},
				new Statement[] {
					new Send("manager", new int[] {23,8,23,49},
						new Performative("request"),
						new Variable(Type.STRING, "Gen"),
						new Predicate("decode", new Term[] {
							new Variable(Type.INTEGER, "Id"),
							new Variable(Type.LIST, "Codes")
						})
					),
					new Wait(
						"manager", new int[] {24,8,25,5},
						new Predicate("decoded", new Term[] {
							new Variable(Type.INTEGER, "Id")
						})
					)
				}
			)
		));
		addRule(new Rule(
			"manager", new int[] {27,9,27,75},
			new MessageEvent(
				new Performative("inform"),
				new Variable(Type.STRING, "F",false),
				new Predicate("codes", new Term[] {
					new Variable(Type.INTEGER, "Id",false),
					new Variable(Type.LIST, "L",false)
				})
			),
			new Predicate("generator", new Term[] {
				new Variable(Type.STRING, "F")
			}),
			new Block(
				"manager", new int[] {27,74,36,5},
				new Statement[] {
					new Declaration(
						new Variable(Type.INTEGER, "Ind"),
						"manager", new int[] {28,8,36,5},
						Primitive.newPrimitive(0)
					),
					new Declaration(
						new Variable(Type.INTEGER, "Max"),
						"manager", new int[] {29,8,36,5},
						new ModuleTerm("prelude", Type.INTEGER,
							new Predicate("size", new Term[] {
								new Variable(Type.LIST, "L")
							}),
							new ModuleTermAdaptor() {
								public Object invoke(Intention intention, Predicate predicate) {
									return ((astra.lang.Prelude) intention.getModule("manager","prelude")).size(
										(astra.term.ListTerm) intention.evaluate(predicate.getTerm(0))
									);
								}
								public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
									return ((astra.lang.Prelude) visitor.agent().getModule("manager","prelude")).size(
										(astra.term.ListTerm) visitor.evaluate(predicate.getTerm(0))
									);
								}
							}
						)
					),
					new While(
						"manager", new int[] {30,8,36,5},
						new Comparison("<",
							new Variable(Type.INTEGER, "Ind"),
							new Variable(Type.INTEGER, "Max")
						),
						new Block(
							"manager", new int[] {30,27,34,9},
							new Statement[] {
								new Declaration(
									new Variable(Type.STRING, "Letter"),
									"manager", new int[] {31,12,34,9},
									new ModuleTerm("prelude", Type.STRING,
										new Predicate("valueAsString", new Term[] {
											new Variable(Type.LIST, "L"),
											new Variable(Type.INTEGER, "Ind")
										}),
										new ModuleTermAdaptor() {
											public Object invoke(Intention intention, Predicate predicate) {
												return ((astra.lang.Prelude) intention.getModule("manager","prelude")).valueAsString(
													(astra.term.ListTerm) intention.evaluate(predicate.getTerm(0)),
													(int) intention.evaluate(predicate.getTerm(1))
												);
											}
											public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
												return ((astra.lang.Prelude) visitor.agent().getModule("manager","prelude")).valueAsString(
													(astra.term.ListTerm) visitor.evaluate(predicate.getTerm(0)),
													(int) visitor.evaluate(predicate.getTerm(1))
												);
											}
										}
									)
								),
								new BeliefUpdate('+',
									"manager", new int[] {32,12,34,9},
									new Predicate("component", new Term[] {
										new Variable(Type.INTEGER, "Id"),
										new Variable(Type.INTEGER, "Ind"),
										new Variable(Type.STRING, "Letter")
									})
								),
								new Assignment(
									new Variable(Type.INTEGER, "Ind"),
									"manager", new int[] {33,12,34,9},
									Operator.newOperator('+',
										new Variable(Type.INTEGER, "Ind"),
										Primitive.newPrimitive(1)
									)
								)
							}
						)
					),
					new BeliefUpdate('+',
						"manager", new int[] {35,8,36,5},
						new Predicate("decoded", new Term[] {
							new Variable(Type.INTEGER, "Id")
						})
					)
				}
			)
		));
		addRule(new Rule(
			"manager", new int[] {38,9,38,78},
			new MessageEvent(
				new Performative("failure"),
				new Variable(Type.STRING, "F",false),
				new Predicate("decode", new Term[] {
					new Variable(Type.INTEGER, "Id",false),
					new Variable(Type.LIST, "C",false)
				})
			),
			new Predicate("generator", new Term[] {
				new Variable(Type.STRING, "F")
			}),
			new Block(
				"manager", new int[] {38,77,41,5},
				new Statement[] {
					new ModuleCall("console",
						"manager", new int[] {39,8,39,60},
						new Predicate("println", new Term[] {
							Operator.newOperator('+',
								Primitive.newPrimitive("Generation Failed: ["),
								Operator.newOperator('+',
									new Variable(Type.INTEGER, "Id"),
									Primitive.newPrimitive("]")
								)
							)
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Console) intention.getModule("manager","console")).println(
									(java.lang.String) intention.evaluate(predicate.getTerm(0))
								);
							}
						}
					),
					new ModuleCall("system",
						"manager", new int[] {40,8,40,21},
						new Predicate("exit", new Term[] {}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.System) intention.getModule("manager","system")).exit(
								);
							}
						}
					)
				}
			)
		));
		addRule(new Rule(
			"manager", new int[] {44,9,44,47},
			new GoalEvent('+',
				new Goal(
					new Predicate("assembled", new Term[] {
						new Variable(Type.INTEGER, "Id",false)
					})
				)
			),
			new NOT(
				new Predicate("decoded", new Term[] {
					new Variable(Type.INTEGER, "Id")
				})
			),
			new Block(
				"manager", new int[] {44,46,47,5},
				new Statement[] {
					new Subgoal(
						"manager", new int[] {45,8,47,5},
						new Goal(
							new Predicate("decoded", new Term[] {
								new Variable(Type.INTEGER, "Id")
							})
						)
					),
					new Subgoal(
						"manager", new int[] {46,8,47,5},
						new Goal(
							new Predicate("assembled", new Term[] {
								new Variable(Type.INTEGER, "Id")
							})
						)
					)
				}
			)
		));
		addRule(new Rule(
			"manager", new int[] {49,9,49,56},
			new GoalEvent('+',
				new Goal(
					new Predicate("assembled", new Term[] {
						new Variable(Type.INTEGER, "Id",false)
					})
				)
			),
			new Predicate("assembler", new Term[] {
				new Variable(Type.STRING, "A",false)
			}),
			new Block(
				"manager", new int[] {49,55,52,5},
				new Statement[] {
					new Send("manager", new int[] {50,8,50,42},
						new Performative("request"),
						new Variable(Type.STRING, "A"),
						new Predicate("assemble", new Term[] {
							new Variable(Type.INTEGER, "Id")
						})
					),
					new Wait(
						"manager", new int[] {51,8,52,5},
						new Predicate("assembled", new Term[] {
							new Variable(Type.INTEGER, "Id"),
							new Variable(Type.STRING, "R",false)
						})
					)
				}
			)
		));
		addRule(new Rule(
			"manager", new int[] {54,9,54,81},
			new MessageEvent(
				new Performative("agree"),
				new Variable(Type.STRING, "From",false),
				new Predicate("assemble", new Term[] {
					new Variable(Type.INTEGER, "Id",false)
				})
			),
			new Predicate("assembler", new Term[] {
				new Variable(Type.STRING, "From")
			}),
			new Block(
				"manager", new int[] {54,80,60,5},
				new Statement[] {
					new BeliefUpdate('+',
						"manager", new int[] {55,8,60,5},
						new Predicate("assembling", new Term[] {
							new Variable(Type.STRING, "From"),
							new Variable(Type.INTEGER, "Id")
						})
					),
					new ForEach(
						"manager", new int[] {56,8,60,5},
						new Predicate("component", new Term[] {
							new Variable(Type.INTEGER, "Id"),
							new Variable(Type.INTEGER, "Index",false),
							new Variable(Type.STRING, "Letter",false)
						}),
						new Block(
							"manager", new int[] {56,61,58,9},
							new Statement[] {
								new Send("manager", new int[] {57,12,57,64},
									new Performative("inform"),
									new Variable(Type.STRING, "From"),
									new Predicate("character", new Term[] {
										new Variable(Type.INTEGER, "Id"),
										new Variable(Type.INTEGER, "Index"),
										new Variable(Type.STRING, "Letter")
									})
								)
							}
						)
					),
					new Send("manager", new int[] {59,8,59,39},
						new Performative("inform"),
						new Variable(Type.STRING, "From"),
						new Predicate("end", new Term[] {
							new Variable(Type.INTEGER, "Id")
						})
					)
				}
			)
		));
		addRule(new Rule(
			"manager", new int[] {62,9,62,88},
			new MessageEvent(
				new Performative("inform"),
				new Variable(Type.STRING, "F",false),
				new Predicate("assembled", new Term[] {
					new Variable(Type.INTEGER, "Id",false),
					new Variable(Type.STRING, "R",false)
				})
			),
			new Predicate("assembling", new Term[] {
				new Variable(Type.STRING, "F"),
				new Variable(Type.INTEGER, "Id")
			}),
			new Block(
				"manager", new int[] {62,87,65,5},
				new Statement[] {
					new BeliefUpdate('-',
						"manager", new int[] {63,8,65,5},
						new Predicate("assembling", new Term[] {
							new Variable(Type.STRING, "F"),
							new Variable(Type.INTEGER, "Id")
						})
					),
					new BeliefUpdate('+',
						"manager", new int[] {64,8,65,5},
						new Predicate("assembled", new Term[] {
							new Variable(Type.INTEGER, "Id"),
							new Variable(Type.STRING, "R")
						})
					)
				}
			)
		));
		addRule(new Rule(
			"manager", new int[] {68,9,68,85},
			new GoalEvent('+',
				new Goal(
					new Predicate("printed", new Term[] {
						new Variable(Type.INTEGER, "Id",false)
					})
				)
			),
			new AND(
				new Predicate("encodedString", new Term[] {
					new Variable(Type.INTEGER, "Id"),
					new Variable(Type.LIST, "L",false)
				}),
				new NOT(
					new Predicate("assembled", new Term[] {
						new Variable(Type.INTEGER, "Id"),
						new Variable(Type.STRING, "R",false)
					})
				)
			),
			new Block(
				"manager", new int[] {68,84,71,5},
				new Statement[] {
					new Subgoal(
						"manager", new int[] {69,8,71,5},
						new Goal(
							new Predicate("assembled", new Term[] {
								new Variable(Type.INTEGER, "Id")
							})
						)
					),
					new Subgoal(
						"manager", new int[] {70,8,71,5},
						new Goal(
							new Predicate("printed", new Term[] {
								new Variable(Type.INTEGER, "Id")
							})
						)
					)
				}
			)
		));
		addRule(new Rule(
			"manager", new int[] {73,9,73,112},
			new GoalEvent('+',
				new Goal(
					new Predicate("printed", new Term[] {
						new Variable(Type.INTEGER, "Id",false)
					})
				)
			),
			new AND(
				new Predicate("encodedString", new Term[] {
					new Variable(Type.INTEGER, "Id"),
					new Variable(Type.LIST, "L",false)
				}),
				new AND(
					new Predicate("assembled", new Term[] {
						new Variable(Type.INTEGER, "Id"),
						new Variable(Type.STRING, "R",false)
					}),
					new Predicate("printer", new Term[] {
						new Variable(Type.STRING, "Printer",false)
					})
				)
			),
			new Block(
				"manager", new int[] {73,111,76,5},
				new Statement[] {
					new Send("manager", new int[] {74,8,74,48},
						new Performative("request"),
						new Variable(Type.STRING, "Printer"),
						new Predicate("print", new Term[] {
							new Variable(Type.INTEGER, "Id"),
							new Variable(Type.STRING, "R")
						})
					),
					new Wait(
						"manager", new int[] {75,8,76,5},
						new Predicate("printed", new Term[] {
							new Variable(Type.INTEGER, "Id")
						})
					)
				}
			)
		));
		addRule(new Rule(
			"manager", new int[] {78,9,78,71},
			new MessageEvent(
				new Performative("inform"),
				new Variable(Type.STRING, "F",false),
				new Predicate("printed", new Term[] {
					new Variable(Type.INTEGER, "Id",false)
				})
			),
			new Predicate("printer", new Term[] {
				new Variable(Type.STRING, "F")
			}),
			new Block(
				"manager", new int[] {78,70,80,5},
				new Statement[] {
					new BeliefUpdate('+',
						"manager", new int[] {79,8,80,5},
						new Predicate("printed", new Term[] {
							new Variable(Type.INTEGER, "Id")
						})
					)
				}
			)
		));
	}

	public void initialize(astra.core.Agent agent) {
		agent.initialize(
			new Predicate("assembler", new Term[] {
				Primitive.newPrimitive("a_agent")
			})
		);
		agent.initialize(
			new Predicate("printer", new Term[] {
				Primitive.newPrimitive("p_agent")
			})
		);
		agent.initialize(
			new Predicate("generator", new Term[] {
				Primitive.newPrimitive("g_agent")
			})
		);
		agent.initialize(
			new Predicate("encodedString", new Term[] {
				Primitive.newPrimitive(1),
				new ListTerm(new Term[] {
					Primitive.newPrimitive(104),
					Primitive.newPrimitive(101),
					Primitive.newPrimitive(108),
					Primitive.newPrimitive(108),
					Primitive.newPrimitive(111)
				})
			})
		);
		agent.initialize(
			new Goal(
				new Predicate("printed", new Term[] {
					Primitive.newPrimitive(1)
				})
			)
		);
	}

	public Fragment createFragment(astra.core.Agent agent) throws ASTRAClassNotFoundException {
		Fragment fragment = new Fragment(this);
		fragment.addModule("console",astra.lang.Console.class,agent);
		fragment.addModule("prelude",astra.lang.Prelude.class,agent);
		fragment.addModule("system",astra.lang.System.class,agent);
		return fragment;
	}

	public static void main(String[] args) {
		Scheduler.setStrategy(new AdaptiveSchedulerStrategy());
		ListTerm argList = new ListTerm();
		for (String arg: args) {
			argList.add(Primitive.newPrimitive(arg));
		}

		String name = java.lang.System.getProperty("astra.name", "main");
		try {
			astra.core.Agent agent = new manager().newInstance(name);
			agent.initialize(new Goal(new Predicate("main", new Term[] { argList })));
			Scheduler.schedule(agent);
		} catch (AgentCreationException e) {
			e.printStackTrace();
		} catch (ASTRAClassNotFoundException e) {
			e.printStackTrace();
		};
	}
}
