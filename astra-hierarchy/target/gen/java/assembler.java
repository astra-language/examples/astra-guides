/**
 * GENERATED CODE - DO NOT CHANGE
 */

import astra.core.*;
import astra.execution.*;
import astra.event.*;
import astra.messaging.*;
import astra.formula.*;
import astra.lang.*;
import astra.statement.*;
import astra.term.*;
import astra.type.*;
import astra.tr.*;
import astra.reasoner.util.*;


public class assembler extends ASTRAClass {
	public assembler() {
		setParents(new Class[] {Shared.class});
		addRule(new Rule(
			"assembler", new int[] {10,9,10,69},
			new MessageEvent(
				new Performative("request"),
				new Variable(Type.STRING, "F",false),
				new Predicate("assemble", new Term[] {
					new Variable(Type.INTEGER, "Id",false)
				})
			),
			new Predicate("manager", new Term[] {
				new Variable(Type.STRING, "F")
			}),
			new Block(
				"assembler", new int[] {10,68,13,5},
				new Statement[] {
					new BeliefUpdate('+',
						"assembler", new int[] {11,8,13,5},
						new Predicate("assembling", new Term[] {
							new Variable(Type.INTEGER, "Id"),
							new Variable(Type.STRING, "F")
						})
					),
					new Send("assembler", new int[] {12,8,12,40},
						new Performative("agree"),
						new Variable(Type.STRING, "F"),
						new Predicate("assemble", new Term[] {
							new Variable(Type.INTEGER, "Id")
						})
					)
				}
			)
		));
		addRule(new Rule(
			"assembler", new int[] {15,9,15,93},
			new MessageEvent(
				new Performative("inform"),
				new Variable(Type.STRING, "F",false),
				new Predicate("character", new Term[] {
					new Variable(Type.INTEGER, "Id",false),
					new Variable(Type.INTEGER, "I",false),
					new Variable(Type.STRING, "L",false)
				})
			),
			new Predicate("assembling", new Term[] {
				new Variable(Type.INTEGER, "Id"),
				new Variable(Type.STRING, "F")
			}),
			new Block(
				"assembler", new int[] {15,92,17,5},
				new Statement[] {
					new BeliefUpdate('+',
						"assembler", new int[] {16,8,17,5},
						new Predicate("component", new Term[] {
							new Variable(Type.INTEGER, "Id"),
							new Variable(Type.INTEGER, "I"),
							new Variable(Type.STRING, "L")
						})
					)
				}
			)
		));
		addRule(new Rule(
			"assembler", new int[] {19,9,19,82},
			new MessageEvent(
				new Performative("inform"),
				new Variable(Type.STRING, "From",false),
				new Predicate("end", new Term[] {
					new Variable(Type.INTEGER, "Id",false)
				})
			),
			new Predicate("assembling", new Term[] {
				new Variable(Type.INTEGER, "Id"),
				new Variable(Type.STRING, "From")
			}),
			new Block(
				"assembler", new int[] {19,81,21,5},
				new Statement[] {
					new Subgoal(
						"assembler", new int[] {20,8,21,5},
						new Goal(
							new Predicate("assemble", new Term[] {
								new Variable(Type.INTEGER, "Id"),
								Primitive.newPrimitive(0),
								Primitive.newPrimitive("")
							})
						)
					)
				}
			)
		));
		addRule(new Rule(
			"assembler", new int[] {23,9,23,92},
			new GoalEvent('+',
				new Goal(
					new Predicate("assemble", new Term[] {
						new Variable(Type.INTEGER, "Id",false),
						new Variable(Type.INTEGER, "Index",false),
						new Variable(Type.STRING, "String",false)
					})
				)
			),
			new Predicate("assembling", new Term[] {
				new Variable(Type.INTEGER, "Id"),
				new Variable(Type.STRING, "Manager",false)
			}),
			new Block(
				"assembler", new int[] {23,91,31,5},
				new Statement[] {
					new If(
						"assembler", new int[] {24,8,31,5},
						new Predicate("component", new Term[] {
							new Variable(Type.INTEGER, "Id"),
							new Variable(Type.INTEGER, "Index"),
							new Variable(Type.STRING, "Letter",false)
						}),
						new Block(
							"assembler", new int[] {24,53,27,9},
							new Statement[] {
								new Declaration(
									new Variable(Type.STRING, "Value"),
									"assembler", new int[] {25,12,27,9},
									Operator.newOperator('+',
										new Variable(Type.STRING, "String"),
										new Variable(Type.STRING, "Letter")
									)
								),
								new Subgoal(
									"assembler", new int[] {26,12,27,9},
									new Goal(
										new Predicate("assemble", new Term[] {
											new Variable(Type.INTEGER, "Id"),
											Operator.newOperator('+',
												new Variable(Type.INTEGER, "Index"),
												Primitive.newPrimitive(1)
											),
											new Variable(Type.STRING, "Value")
										})
									)
								)
							}
						),
						new Block(
							"assembler", new int[] {28,13,31,5},
							new Statement[] {
								new BeliefUpdate('+',
									"assembler", new int[] {29,12,30,9},
									new Predicate("assembled", new Term[] {
										new Variable(Type.INTEGER, "Id"),
										new Variable(Type.STRING, "String")
									})
								)
							}
						)
					)
				}
			)
		));
		addRule(new Rule(
			"assembler", new int[] {33,9,33,77},
			new BeliefEvent('+',
				new Predicate("assembled", new Term[] {
					new Variable(Type.INTEGER, "Id",false),
					new Variable(Type.STRING, "String",false)
				})
			),
			new Predicate("assembling", new Term[] {
				new Variable(Type.INTEGER, "Id"),
				new Variable(Type.STRING, "Manager",false)
			}),
			new Block(
				"assembler", new int[] {33,76,35,5},
				new Statement[] {
					new Send("assembler", new int[] {34,8,34,56},
						new Performative("inform"),
						new Variable(Type.STRING, "Manager"),
						new Predicate("assembled", new Term[] {
							new Variable(Type.INTEGER, "Id"),
							new Variable(Type.STRING, "String")
						})
					)
				}
			)
		));
	}

	public void initialize(astra.core.Agent agent) {
		agent.initialize(
			new Predicate("manager", new Term[] {
				Primitive.newPrimitive("m_agent")
			})
		);
	}

	public Fragment createFragment(astra.core.Agent agent) throws ASTRAClassNotFoundException {
		Fragment fragment = new Fragment(this);
		fragment.addModule("console",astra.lang.Console.class,agent);
		return fragment;
	}

	public static void main(String[] args) {
		Scheduler.setStrategy(new AdaptiveSchedulerStrategy());
		ListTerm argList = new ListTerm();
		for (String arg: args) {
			argList.add(Primitive.newPrimitive(arg));
		}

		String name = java.lang.System.getProperty("astra.name", "main");
		try {
			astra.core.Agent agent = new assembler().newInstance(name);
			agent.initialize(new Goal(new Predicate("main", new Term[] { argList })));
			Scheduler.schedule(agent);
		} catch (AgentCreationException e) {
			e.printStackTrace();
		} catch (ASTRAClassNotFoundException e) {
			e.printStackTrace();
		};
	}
}
