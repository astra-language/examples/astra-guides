/**
 * GENERATED CODE - DO NOT CHANGE
 */

import astra.core.*;
import astra.execution.*;
import astra.event.*;
import astra.messaging.*;
import astra.formula.*;
import astra.lang.*;
import astra.statement.*;
import astra.term.*;
import astra.type.*;
import astra.tr.*;
import astra.reasoner.util.*;


public class generator extends ASTRAClass {
	public generator() {
		setParents(new Class[] {Shared.class});
		addRule(new Rule(
			"generator", new int[] {6,8,7,21},
			new MessageEvent(
				new Performative("request"),
				new Variable(Type.STRING, "F",false),
				new Predicate("decode", new Term[] {
					new Variable(Type.INTEGER, "Id",false),
					new Variable(Type.LIST, "Codes",false)
				})
			),
			new Predicate("manager", new Term[] {
				new Variable(Type.STRING, "F")
			}),
			new Block(
				"generator", new int[] {7,20,16,4},
				new Statement[] {
					new Send("generator", new int[] {8,8,8,44},
						new Performative("agree"),
						new Variable(Type.STRING, "F"),
						new Predicate("decode", new Term[] {
							new Variable(Type.INTEGER, "Id"),
							new Variable(Type.LIST, "Codes")
						})
					),
					new TryRecover(
						"generator", new int[] {9,8,16,4},
						new Block(
							"generator", new int[] {9,12,12,10},
							new Statement[] {
								new ModuleCall("library",
									"generator", new int[] {10,12,10,49},
									new Predicate("lookup", new Term[] {
										new Variable(Type.LIST, "Codes"),
										new Variable(Type.LIST, "Letters",false)
									}),
									new DefaultModuleCallAdaptor() {
										public boolean inline() {
											return true;
										}

										public boolean invoke(Intention intention, Predicate predicate) {
											return ((LookupLibrary) intention.getModule("generator","library")).lookup(
												(astra.term.ListTerm) intention.evaluate(predicate.getTerm(0)),
												(ActionParam<astra.term.ListTerm>) intention.evaluate(predicate.getTerm(1))
											);
										}
									}
								),
								new Send("generator", new int[] {11,12,11,51},
									new Performative("inform"),
									new Variable(Type.STRING, "F"),
									new Predicate("codes", new Term[] {
										new Variable(Type.INTEGER, "Id"),
										new Variable(Type.LIST, "Letters")
									})
								)
							}
						),
						new Block(
							"generator", new int[] {13,17,16,4},
							new Statement[] {
								new Send("generator", new int[] {14,12,14,51},
									new Performative("failure"),
									new Variable(Type.STRING, "F"),
									new Predicate("decode", new Term[] {
										new Variable(Type.INTEGER, "Id"),
										new Variable(Type.LIST, "Codes")
									})
								)
							}
						)
					)
				}
			)
		));
	}

	public void initialize(astra.core.Agent agent) {
		agent.initialize(
			new Predicate("manager", new Term[] {
				Primitive.newPrimitive("m_agent")
			})
		);
	}

	public Fragment createFragment(astra.core.Agent agent) throws ASTRAClassNotFoundException {
		Fragment fragment = new Fragment(this);
		fragment.addModule("library",LookupLibrary.class,agent);
		return fragment;
	}

	public static void main(String[] args) {
		Scheduler.setStrategy(new AdaptiveSchedulerStrategy());
		ListTerm argList = new ListTerm();
		for (String arg: args) {
			argList.add(Primitive.newPrimitive(arg));
		}

		String name = java.lang.System.getProperty("astra.name", "main");
		try {
			astra.core.Agent agent = new generator().newInstance(name);
			agent.initialize(new Goal(new Predicate("main", new Term[] { argList })));
			Scheduler.schedule(agent);
		} catch (AgentCreationException e) {
			e.printStackTrace();
		} catch (ASTRAClassNotFoundException e) {
			e.printStackTrace();
		};
	}
}
