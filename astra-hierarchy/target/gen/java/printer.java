/**
 * GENERATED CODE - DO NOT CHANGE
 */

import astra.core.*;
import astra.execution.*;
import astra.event.*;
import astra.messaging.*;
import astra.formula.*;
import astra.lang.*;
import astra.statement.*;
import astra.term.*;
import astra.type.*;
import astra.tr.*;
import astra.reasoner.util.*;


public class printer extends ASTRAClass {
	public printer() {
		setParents(new Class[] {Shared.class});
		addRule(new Rule(
			"printer", new int[] {6,8,6,76},
			new MessageEvent(
				new Performative("request"),
				new Variable(Type.STRING, "F",false),
				new Predicate("print", new Term[] {
					new Variable(Type.INTEGER, "Id",false),
					new Variable(Type.STRING, "R",false)
				})
			),
			new Predicate("manager", new Term[] {
				new Variable(Type.STRING, "F")
			}),
			new Block(
				"printer", new int[] {6,75,10,4},
				new Statement[] {
					new Send("printer", new int[] {7,6,7,35},
						new Performative("agree"),
						new Variable(Type.STRING, "F"),
						new Predicate("print", new Term[] {
							new Variable(Type.INTEGER, "Id"),
							new Variable(Type.STRING, "R")
						})
					),
					new ModuleCall("console",
						"printer", new int[] {8,6,8,60},
						new Predicate("println", new Term[] {
							Operator.newOperator('+',
								Primitive.newPrimitive("Result of job "),
								Operator.newOperator('+',
									new Variable(Type.INTEGER, "Id"),
									Operator.newOperator('+',
										Primitive.newPrimitive(" is:["),
										Operator.newOperator('+',
											new Variable(Type.STRING, "R"),
											Primitive.newPrimitive("]")
										)
									)
								)
							)
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Console) intention.getModule("printer","console")).println(
									(java.lang.String) intention.evaluate(predicate.getTerm(0))
								);
							}
						}
					),
					new Send("printer", new int[] {9,6,9,38},
						new Performative("inform"),
						new Variable(Type.STRING, "F"),
						new Predicate("printed", new Term[] {
							new Variable(Type.INTEGER, "Id")
						})
					)
				}
			)
		));
	}

	public void initialize(astra.core.Agent agent) {
		agent.initialize(
			new Predicate("manager", new Term[] {
				Primitive.newPrimitive("m_agent")
			})
		);
	}

	public Fragment createFragment(astra.core.Agent agent) throws ASTRAClassNotFoundException {
		Fragment fragment = new Fragment(this);
		fragment.addModule("console",astra.lang.Console.class,agent);
		return fragment;
	}

	public static void main(String[] args) {
		Scheduler.setStrategy(new AdaptiveSchedulerStrategy());
		ListTerm argList = new ListTerm();
		for (String arg: args) {
			argList.add(Primitive.newPrimitive(arg));
		}

		String name = java.lang.System.getProperty("astra.name", "main");
		try {
			astra.core.Agent agent = new printer().newInstance(name);
			agent.initialize(new Goal(new Predicate("main", new Term[] { argList })));
			Scheduler.schedule(agent);
		} catch (AgentCreationException e) {
			e.printStackTrace();
		} catch (ASTRAClassNotFoundException e) {
			e.printStackTrace();
		};
	}
}
