

import astra.core.ActionParam;
import astra.core.Module;
import astra.term.ListTerm;
import astra.term.Primitive;
import astra.term.Term;

public class LookupLibrary extends Module {
   @ACTION
   public boolean lookup(ListTerm codes, ActionParam<ListTerm> letters) {
      Term[] terms = new Term[codes.size()];
      for (int i=0; i<terms.length; i++) {
         Integer val = (Integer) ((Primitive<?>) codes.get(i)).value();
         if (32 <= val && val <= 126) {
            String letter = Character.toString( (char) val.intValue() );
            terms[i] = Primitive.newPrimitive(letter);
         } else {
            return false;
         }
      }
      letters.set(new ListTerm(terms));
      return true;
   }
}
