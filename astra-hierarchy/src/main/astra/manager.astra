agent manager extends Shared {
	module Console console;
	module Prelude prelude;
	module System system;
	
	types manager {
		formula printing(int, string);
	}
	
	// initial beliefs about production agents
	initial assembler( "a_agent" );
	initial printer( "p_agent" );
	initial generator( "g_agent" );

	// initial belief about a string to construct
	initial encodedString( 1, [ 104, 101, 108, 108, 111 ] );

	// goal to begin the process of looking up word 1
	initial !printed( 1 );

	//--------- INTERACTION WITH GENERATOR
	rule +!decoded( int Id ) : ~decoded(Id) & generator( string Gen ) & encodedString( Id, list Codes ) {
		send( request, Gen, decode( Id, Codes ) );
		wait(decoded(Id));
	}
		
	rule @message(inform, string F, codes(int Id, list L)) : generator(F) {
		int Ind = 0;
		int Max = prelude.size(L);
		while( Ind < Max ) {
			string Letter = prelude.valueAsString(L, Ind);
			+component( Id, Ind, Letter );
			Ind = Ind + 1;
		}
		+decoded(Id);
	}
	
	rule @message(failure, string F, decode( int Id, list C)) : generator(F) {
		console.println( "Generation Failed: [" + Id + "]" );
		system.exit();
	}
	
	//--------- INTERACTION WITH ASSEMBLER
	rule +!assembled( int Id ) : ~decoded(Id) {
		!decoded(Id);
		!assembled(Id);
	}

	rule +!assembled( int Id ) : assembler( string A ) {
		send( request, A, assemble( Id ) );
		wait(assembled(Id, string R));
	}
	
	rule @message( agree, string From, assemble( int Id ) ) : assembler( From ) {
		+assembling( From, Id );
		foreach( component( Id, int Index, string Letter ) ) {
			send( inform, From, character( Id, Index, Letter ) );
		}
		send( inform, From, end( Id ) );
	}
	
	rule @message(inform, string F, assembled(int Id, string R)) : assembling( F, Id ) {
		-assembling( F, Id );
		+assembled( Id, R) ;
	}
	
	//--------- INTERACTION WITH PRINTER
	rule +!printed( int Id ) : encodedString(Id, list L) & ~assembled(Id, string R) {
		!assembled( Id );
		!printed( Id );
	}

	rule +!printed( int Id ) : encodedString(Id, list L) & assembled(Id, string R) & printer( string Printer ) {
		send( request, Printer, print( Id, R ) );
		wait(printed(Id));
	}
	
	rule @message(inform, string F, printed( int Id )) : printer( F ) {
		+printed(Id);
	}
} // this closes the original 'agent' definition
