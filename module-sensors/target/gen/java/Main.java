/**
 * GENERATED CODE - DO NOT CHANGE
 */

import astra.core.*;
import astra.execution.*;
import astra.event.*;
import astra.messaging.*;
import astra.formula.*;
import astra.lang.*;
import astra.statement.*;
import astra.term.*;
import astra.type.*;
import astra.tr.*;
import astra.reasoner.util.*;


public class Main extends ASTRAClass {
	public Main() {
		setParents(new Class[] {astra.lang.Agent.class});
		addRule(new Rule(
			"Main", new int[] {9,7,9,26},
			new GoalEvent('+',
				new Goal(
					new Predicate("main", new Term[] {
						new Variable(Type.LIST, "args",false)
					})
				)
			),
			Predicate.TRUE,
			new Block(
				"Main", new int[] {9,25,12,3},
				new Statement[] {
					new Subgoal(
						"Main", new int[] {10,4,12,3},
						new Goal(
							new Predicate("flash", new Term[] {
								Primitive.newPrimitive(3)
							})
						)
					),
					new ModuleCall("console",
						"Main", new int[] {11,4,11,31},
						new Predicate("println", new Term[] {
							Primitive.newPrimitive("FINISHED")
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Console) intention.getModule("Main","console")).println(
									(java.lang.String) intention.evaluate(predicate.getTerm(0))
								);
							}
						}
					)
				}
			)
		));
		addRule(new Rule(
			"Main", new int[] {14,7,14,30},
			new GoalEvent('+',
				new Goal(
					new Predicate("flash", new Term[] {
						new Variable(Type.INTEGER, "N",false)
					})
				)
			),
			new Comparison(">",
				new Variable(Type.INTEGER, "N"),
				Primitive.newPrimitive(0)
			),
			new Block(
				"Main", new int[] {14,29,18,3},
				new Statement[] {
					new Subgoal(
						"Main", new int[] {15,4,18,3},
						new Goal(
							new Predicate("switch", new Term[] {
								Primitive.newPrimitive("on")
							})
						)
					),
					new Subgoal(
						"Main", new int[] {16,4,18,3},
						new Goal(
							new Predicate("switch", new Term[] {
								Primitive.newPrimitive("off")
							})
						)
					),
					new Subgoal(
						"Main", new int[] {17,4,18,3},
						new Goal(
							new Predicate("flash", new Term[] {
								Operator.newOperator('-',
									new Variable(Type.INTEGER, "N"),
									Primitive.newPrimitive(1)
								)
							})
						)
					)
				}
			)
		));
		addRule(new Rule(
			"Main", new int[] {20,7,20,23},
			new GoalEvent('+',
				new Goal(
					new Predicate("flash", new Term[] {
						new Variable(Type.INTEGER, "N",false)
					})
				)
			),
			Predicate.TRUE,
			new Block(
				"Main", new int[] {20,22,20,24},
				new Statement[] {
				}
			)
		));
		addRule(new Rule(
			"Main", new int[] {22,7,22,48},
			new GoalEvent('+',
				new Goal(
					new Predicate("switch", new Term[] {
						new Variable(Type.STRING, "state",false)
					})
				)
			),
			new NOT(
				new Predicate("switch", new Term[] {
					new Variable(Type.STRING, "state")
				})
			),
			new Block(
				"Main", new int[] {22,47,25,3},
				new Statement[] {
					new ModuleCall("switch",
						"Main", new int[] {23,4,23,17},
						new Predicate("flip", new Term[] {}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((Switch) intention.getModule("Main","switch")).flip(
								);
							}
						}
					),
					new Wait(
						"Main", new int[] {24,4,25,3},
						new Predicate("switch", new Term[] {
							new Variable(Type.STRING, "state")
						})
					)
				}
			)
		));
		addRule(new Rule(
			"Main", new int[] {27,7,27,31},
			new GoalEvent('+',
				new Goal(
					new Predicate("switch", new Term[] {
						new Variable(Type.STRING, "state",false)
					})
				)
			),
			Predicate.TRUE,
			new Block(
				"Main", new int[] {27,30,29,3},
				new Statement[] {
					new ModuleCall("console",
						"Main", new int[] {28,4,28,40},
						new Predicate("println", new Term[] {
							Primitive.newPrimitive("should not happen")
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Console) intention.getModule("Main","console")).println(
									(java.lang.String) intention.evaluate(predicate.getTerm(0))
								);
							}
						}
					)
				}
			)
		));
		addRule(new Rule(
			"Main", new int[] {31,7,31,30},
			new BeliefEvent('+',
				new Predicate("switch", new Term[] {
					new Variable(Type.STRING, "state",false)
				})
			),
			Predicate.TRUE,
			new Block(
				"Main", new int[] {31,29,33,3},
				new Statement[] {
					new ModuleCall("console",
						"Main", new int[] {32,4,32,38},
						new Predicate("println", new Term[] {
							Operator.newOperator('+',
								Primitive.newPrimitive("switch="),
								new Variable(Type.STRING, "state")
							)
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Console) intention.getModule("Main","console")).println(
									(java.lang.String) intention.evaluate(predicate.getTerm(0))
								);
							}
						}
					)
				}
			)
		));
	}

	public void initialize(astra.core.Agent agent) {
		agent.addSensorAdaptor(new SensorAdaptor() {
			public void sense(astra.core.Agent agent) {
				((Switch) agent.getModule("Main","switch")).switchSensor();
			}
		});

	}

	public Fragment createFragment(astra.core.Agent agent) throws ASTRAClassNotFoundException {
		Fragment fragment = new Fragment(this);
		fragment.addModule("switch",Switch.class,agent);
		fragment.addModule("console",astra.lang.Console.class,agent);
		return fragment;
	}

	public static void main(String[] args) {
		Scheduler.setStrategy(new AdaptiveSchedulerStrategy());
		ListTerm argList = new ListTerm();
		for (String arg: args) {
			argList.add(Primitive.newPrimitive(arg));
		}

		String name = java.lang.System.getProperty("astra.name", "main");
		try {
			astra.core.Agent agent = new Main().newInstance(name);
			agent.initialize(new Goal(new Predicate("main", new Term[] { argList })));
			Scheduler.schedule(agent);
		} catch (AgentCreationException e) {
			e.printStackTrace();
		} catch (ASTRAClassNotFoundException e) {
			e.printStackTrace();
		};
	}
}
