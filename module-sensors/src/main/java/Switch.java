import astra.core.Agent;
import astra.core.Module;
import astra.formula.Predicate;
import astra.term.Primitive;
import astra.term.Term;

public class Switch extends Module {
  private boolean on = false;
  private boolean lastOn = true;
  private Predicate belief;

  public void setAgent(Agent agent) {
    super.setAgent(agent);
  }

  @ACTION
  public boolean flip() {
    on = !on;
    return true;
  }

  @SENSOR
  public void switchSensor() {
    if (on != lastOn) {
      if (belief != null) {
        agent.beliefs().dropBelief(belief);
      }
      belief = new Predicate("switch", new Term[] {
          Primitive.newPrimitive(on ? "on":"off")
      });
      agent.beliefs().addBelief(belief);
      lastOn = on;
    }
  }
}