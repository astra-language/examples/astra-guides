package jms;


import java.util.HashMap;
import java.util.Map;

import astra.core.Agent;
import astra.reasoner.EventUnifier;
import astra.reasoner.Unifier;
import astra.term.Term;

public class JMSEventUnifier implements EventUnifier<JMSEvent> {

	@Override
	public Map<Integer, Term> unify(JMSEvent source, JMSEvent target, Agent agent) {
		return Unifier.unify(
				new Term[] {source.type, source.source, source.content}, 
				new Term[] {target.type, target.source, target.content}, 
				new HashMap<Integer, Term>(),
				agent);
	}


}
