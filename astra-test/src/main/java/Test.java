import astra.core.Module;
import astra.term.Funct;
import astra.term.ListTerm;
import astra.term.Term;

public class Test extends Module {
    @TERM
    public Funct getFunction(String funct, ListTerm args) {
        return new Funct(funct, args.toArray(new Term[args.size()]));
    }

    @TERM
    public String getString(String val) {
        return val+"**";
    }
}
