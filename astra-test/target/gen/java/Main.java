/**
 * GENERATED CODE - DO NOT CHANGE
 */

import astra.core.*;
import astra.execution.*;
import astra.event.*;
import astra.messaging.*;
import astra.formula.*;
import astra.lang.*;
import astra.statement.*;
import astra.term.*;
import astra.type.*;
import astra.tr.*;
import astra.reasoner.util.*;


public class Main extends ASTRAClass {
	public Main() {
		setParents(new Class[] {astra.lang.Agent.class});
		addRule(new Rule(
			"Main", new int[] {11,9,11,28},
			new GoalEvent('+',
				new Goal(
					new Predicate("main", new Term[] {
						new Variable(Type.LIST, "args",false)
					})
				)
			),
			Predicate.TRUE,
			new Block(
				"Main", new int[] {11,27,15,5},
				new Statement[] {
					new Subgoal(
						"Main", new int[] {12,8,15,5},
						new Goal(
							new Predicate("test", new Term[] {})
						)
					),
					new SpecialBeliefUpdate(
						"Main", new int[] {13,8,15,5},
						new Predicate("val", new Term[] {
							Primitive.newPrimitive("bar")
						})
					),
					new Subgoal(
						"Main", new int[] {14,8,15,5},
						new Goal(
							new Predicate("test", new Term[] {})
						)
					)
				}
			)
		));
		addRule(new Rule(
			"Main", new int[] {17,9,17,35},
			new GoalEvent('+',
				new Goal(
					new Predicate("test", new Term[] {})
				)
			),
			new Predicate("val", new Term[] {
				new Variable(Type.STRING, "X",false)
			}),
			new Block(
				"Main", new int[] {17,34,20,5},
				new Statement[] {
					new Declaration(
						new Variable(Type.FUNCTION, "F"),
						"Main", new int[] {18,8,20,5},
						new ModuleTerm("test", Type.FUNCTION,
							new Predicate("getFunction", new Term[] {
								Primitive.newPrimitive("f"),
								new ListTerm(new Term[] {
									new ModuleTerm("test", Type.STRING,
										new Predicate("getString", new Term[] {
											new Variable(Type.STRING, "X")
										}),
										new ModuleTermAdaptor() {
											public Object invoke(Intention intention, Predicate predicate) {
												return ((Test) intention.getModule("Main","test")).getString(
													(java.lang.String) intention.evaluate(predicate.getTerm(0))
												);
											}
											public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
												return ((Test) visitor.agent().getModule("Main","test")).getString(
													(java.lang.String) visitor.evaluate(predicate.getTerm(0))
												);
											}
										}
									)
								})
							}),
							new ModuleTermAdaptor() {
								public Object invoke(Intention intention, Predicate predicate) {
									return ((Test) intention.getModule("Main","test")).getFunction(
										(java.lang.String) intention.evaluate(predicate.getTerm(0)),
										(astra.term.ListTerm) intention.evaluate(predicate.getTerm(1))
									);
								}
								public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
									return ((Test) visitor.agent().getModule("Main","test")).getFunction(
										(java.lang.String) visitor.evaluate(predicate.getTerm(0)),
										(astra.term.ListTerm) visitor.evaluate(predicate.getTerm(1))
									);
								}
							}
						)
					),
					new ModuleCall("console",
						"Main", new int[] {19,8,19,31},
						new Predicate("println", new Term[] {
							Operator.newOperator('+',
								Primitive.newPrimitive("F="),
								new Variable(Type.FUNCTION, "F")
							)
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Console) intention.getModule("Main","console")).println(
									(java.lang.String) intention.evaluate(predicate.getTerm(0))
								);
							}
						}
					)
				}
			)
		));
	}

	public void initialize(astra.core.Agent agent) {
		agent.initialize(
			new Predicate("val", new Term[] {
				Primitive.newPrimitive("hey")
			})
		);
	}

	public Fragment createFragment(astra.core.Agent agent) throws ASTRAClassNotFoundException {
		Fragment fragment = new Fragment(this);
		fragment.addModule("console",astra.lang.Console.class,agent);
		fragment.addModule("test",Test.class,agent);
		return fragment;
	}

	public static void main(String[] args) {
		Scheduler.setStrategy(new TestSchedulerStrategy());
		ListTerm argList = new ListTerm();
		for (String arg: args) {
			argList.add(Primitive.newPrimitive(arg));
		}

		String name = java.lang.System.getProperty("astra.name", "main");
		try {
			astra.core.Agent agent = new Main().newInstance(name);
			agent.initialize(new Goal(new Predicate("main", new Term[] { argList })));
			Scheduler.schedule(agent);
		} catch (AgentCreationException e) {
			e.printStackTrace();
		} catch (ASTRAClassNotFoundException e) {
			e.printStackTrace();
		};
	}
}
