package gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

import astra.gui.AstraEventListener;
import astra.gui.AstraGui;

public class LightSwitchGui extends JFrame implements AstraGui {

    /**
     *
     */
    private static final long serialVersionUID = 484171540807763682L;
    AstraEventListener listener;
    private boolean state = false;
    private JTextField light = new JTextField("OFF");

    public LightSwitchGui() {
        setLayout(new java.awt.BorderLayout());
        JButton button = new JButton("Switch");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                state = !state;
                listener.addEvent("switch", new Object[] {state ? "on":"off"});
            }
        });
        add(BorderLayout.WEST, button);
        add(BorderLayout.EAST, light);
        addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                listener.addEvent("close", new Object[0]);
            }
        });        
        pack();
    }

    @Override
    public boolean receive(String type, List<?> args) {
        if (type.equals("light")) {
            light.setText(args.get(0).toString());
            return true;
        }
        return false;
    }

    @Override
    public void launch(AstraEventListener listener) {
        this.listener = listener;
        setVisible(true);
    }
    
}
