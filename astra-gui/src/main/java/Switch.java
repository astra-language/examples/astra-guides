import astra.core.Agent;
import astra.core.Module;
import astra.event.Event;
import astra.formula.Predicate;
import astra.term.Primitive;
import astra.term.Term;

public class Switch extends Module {
  private boolean on = false;
  private Predicate belief;

  public void setAgent(Agent agent) {
    super.setAgent(agent);
    updateBeliefs();
  }

  @ACTION
  public boolean flip() {
    on = !on;
    updateBeliefs();
    return true;
  }

  private void updateBeliefs() {
    if (belief != null) {
      agent.beliefs().dropBelief(belief);
    }
    belief = new Predicate("switch", new Term[] {
        Primitive.newPrimitive(on ? "on":"off")
    });
    agent.beliefs().addBelief(belief);
  }
}