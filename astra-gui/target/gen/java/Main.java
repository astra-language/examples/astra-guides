/**
 * GENERATED CODE - DO NOT CHANGE
 */

import astra.core.*;
import astra.execution.*;
import astra.event.*;
import astra.messaging.*;
import astra.formula.*;
import astra.lang.*;
import astra.statement.*;
import astra.term.*;
import astra.type.*;
import astra.tr.*;
import astra.reasoner.util.*;


public class Main extends ASTRAClass {
	public Main() {
		setParents(new Class[] {astra.lang.Agent.class});
		addRule(new Rule(
			"Main", new int[] {12,7,12,26},
			new GoalEvent('+',
				new Goal(
					new Predicate("main", new Term[] {
						new Variable(Type.LIST, "args",false)
					})
				)
			),
			Predicate.TRUE,
			new Block(
				"Main", new int[] {12,25,20,3},
				new Statement[] {
					new Declaration(
						new Variable(Type.BOOLEAN, "val"),
						"Main", new int[] {13,4,20,3},
						Primitive.newPrimitive(true)
					),
					new SpawnGoal(
						"Main", new int[] {14,4,20,3},
						new Goal(
							new Predicate("ex", new Term[] {
								new Variable(Type.BOOLEAN, "val")
							})
						)
					),
					new ModuleCall("console",
						"Main", new int[] {15,4,15,29},
						new Predicate("println", new Term[] {
							Primitive.newPrimitive("before")
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Console) intention.getModule("Main","console")).println(
									(java.lang.String) intention.evaluate(predicate.getTerm(0))
								);
							}
						}
					),
					new Wait(
						"Main", new int[] {16,4,20,3},
						new Predicate("test", new Term[] {
							new Funct("happy", new Term[] {
								new Variable(Type.BOOLEAN, "val")
							})
						})
					),
					new ModuleCall("console",
						"Main", new int[] {17,4,17,28},
						new Predicate("println", new Term[] {
							Primitive.newPrimitive("after")
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Console) intention.getModule("Main","console")).println(
									(java.lang.String) intention.evaluate(predicate.getTerm(0))
								);
							}
						}
					),
					new ModuleCall("system",
						"Main", new int[] {19,4,19,17},
						new Predicate("exit", new Term[] {}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.System) intention.getModule("Main","system")).exit(
								);
							}
						}
					)
				}
			)
		));
		addRule(new Rule(
			"Main", new int[] {22,7,22,26},
			new GoalEvent('+',
				new Goal(
					new Predicate("ex", new Term[] {
						new Variable(Type.BOOLEAN, "val",false)
					})
				)
			),
			Predicate.TRUE,
			new Block(
				"Main", new int[] {22,25,27,3},
				new Statement[] {
					new ModuleCall("console",
						"Main", new int[] {23,4,23,30},
						new Predicate("println", new Term[] {
							Primitive.newPrimitive("in ex()")
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Console) intention.getModule("Main","console")).println(
									(java.lang.String) intention.evaluate(predicate.getTerm(0))
								);
							}
						}
					),
					new ModuleCall("system",
						"Main", new int[] {24,4,24,21},
						new Predicate("sleep", new Term[] {
							Primitive.newPrimitive(1000)
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.System) intention.getModule("Main","system")).sleep(
									(java.lang.Integer) intention.evaluate(predicate.getTerm(0))
								);
							}
						}
					),
					new Send("Main", new int[] {25,4,25,49},
						new Performative("inform"),
						new ModuleTerm("system", Type.STRING,
							new Predicate("name", new Term[] {}),
							new ModuleTermAdaptor() {
								public Object invoke(Intention intention, Predicate predicate) {
									return ((astra.lang.System) intention.getModule("Main","system")).name(
									);
								}
								public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
									return ((astra.lang.System) visitor.agent().getModule("Main","system")).name(
									);
								}
							}
						),
						new Predicate("test", new Term[] {
							new Funct("happy", new Term[] {
								new Variable(Type.BOOLEAN, "val")
							})
						})
					),
					new ModuleCall("console",
						"Main", new int[] {26,4,26,31},
						new Predicate("println", new Term[] {
							Primitive.newPrimitive("out ex()")
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Console) intention.getModule("Main","console")).println(
									(java.lang.String) intention.evaluate(predicate.getTerm(0))
								);
							}
						}
					)
				}
			)
		));
		addRule(new Rule(
			"Main", new int[] {29,7,29,66},
			new MessageEvent(
				new Performative("inform"),
				new Variable(Type.STRING, "sender",false),
				new Predicate("test", new Term[] {
					new Funct("happy", new Term[] {
						new Variable(Type.BOOLEAN, "val",false)
					})
				})
			),
			Predicate.TRUE,
			new Block(
				"Main", new int[] {29,65,33,3},
				new Statement[] {
					new ModuleCall("console",
						"Main", new int[] {30,4,30,34},
						new Predicate("println", new Term[] {
							Primitive.newPrimitive("got message")
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Console) intention.getModule("Main","console")).println(
									(java.lang.String) intention.evaluate(predicate.getTerm(0))
								);
							}
						}
					),
					new BeliefUpdate('+',
						"Main", new int[] {31,4,33,3},
						new Predicate("test", new Term[] {
							new Funct("happy", new Term[] {
								new Variable(Type.BOOLEAN, "val")
							})
						})
					),
					new ModuleCall("console",
						"Main", new int[] {32,4,32,35},
						new Predicate("println", new Term[] {
							Primitive.newPrimitive("tested happy")
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Console) intention.getModule("Main","console")).println(
									(java.lang.String) intention.evaluate(predicate.getTerm(0))
								);
							}
						}
					)
				}
			)
		));
		addRule(new Rule(
			"Main", new int[] {35,7,35,37},
			new ModuleEvent("gui",
				"$gui:",
				new Predicate("event", new Term[] {
					Primitive.newPrimitive("switch"),
					new ListTerm(new Term[] {
						Primitive.newPrimitive("on")
					})
				}),
				new ModuleEventAdaptor() {
					public Event generate(astra.core.Agent agent, Predicate predicate) {
						return ((astra.gui.GuiModule) agent.getModule("Main","gui")).event(
							predicate.getTerm(0),
							predicate.getTerm(1)
						);
					}
				}
			),
			Predicate.TRUE,
			new Block(
				"Main", new int[] {35,36,37,3},
				new Statement[] {
					new ModuleCall("gui",
						"Main", new int[] {36,4,36,34},
						new Predicate("updateGui", new Term[] {
							Primitive.newPrimitive("light"),
							new ListTerm(new Term[] {
								Primitive.newPrimitive("ON")
							})
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.gui.GuiModule) intention.getModule("Main","gui")).updateGui(
									(java.lang.String) intention.evaluate(predicate.getTerm(0)),
									(astra.term.ListTerm) intention.evaluate(predicate.getTerm(1))
								);
							}
						}
					)
				}
			)
		));
		addRule(new Rule(
			"Main", new int[] {39,7,39,38},
			new ModuleEvent("gui",
				"$gui:",
				new Predicate("event", new Term[] {
					Primitive.newPrimitive("switch"),
					new ListTerm(new Term[] {
						Primitive.newPrimitive("off")
					})
				}),
				new ModuleEventAdaptor() {
					public Event generate(astra.core.Agent agent, Predicate predicate) {
						return ((astra.gui.GuiModule) agent.getModule("Main","gui")).event(
							predicate.getTerm(0),
							predicate.getTerm(1)
						);
					}
				}
			),
			Predicate.TRUE,
			new Block(
				"Main", new int[] {39,37,41,3},
				new Statement[] {
					new ModuleCall("gui",
						"Main", new int[] {40,4,40,35},
						new Predicate("updateGui", new Term[] {
							Primitive.newPrimitive("light"),
							new ListTerm(new Term[] {
								Primitive.newPrimitive("OFF")
							})
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.gui.GuiModule) intention.getModule("Main","gui")).updateGui(
									(java.lang.String) intention.evaluate(predicate.getTerm(0)),
									(astra.term.ListTerm) intention.evaluate(predicate.getTerm(1))
								);
							}
						}
					)
				}
			)
		));
		addRule(new Rule(
			"Main", new int[] {43,7,43,32},
			new ModuleEvent("gui",
				"$gui:",
				new Predicate("event", new Term[] {
					Primitive.newPrimitive("close"),
					new ListTerm(new Term[] {

					})
				}),
				new ModuleEventAdaptor() {
					public Event generate(astra.core.Agent agent, Predicate predicate) {
						return ((astra.gui.GuiModule) agent.getModule("Main","gui")).event(
							predicate.getTerm(0),
							predicate.getTerm(1)
						);
					}
				}
			),
			Predicate.TRUE,
			new Block(
				"Main", new int[] {43,31,45,3},
				new Statement[] {
					new ModuleCall("system",
						"Main", new int[] {44,4,44,17},
						new Predicate("exit", new Term[] {}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.System) intention.getModule("Main","system")).exit(
								);
							}
						}
					)
				}
			)
		));
	}

	public void initialize(astra.core.Agent agent) {
	}

	public Fragment createFragment(astra.core.Agent agent) throws ASTRAClassNotFoundException {
		Fragment fragment = new Fragment(this);
		fragment.addModule("system",astra.lang.System.class,agent);
		fragment.addModule("console",astra.lang.Console.class,agent);
		fragment.addModule("debug",astra.lang.Debug.class,agent);
		fragment.addModule("gui",astra.gui.GuiModule.class,agent);
		return fragment;
	}

	public static void main(String[] args) {
		Scheduler.setStrategy(new TestSchedulerStrategy());
		ListTerm argList = new ListTerm();
		for (String arg: args) {
			argList.add(Primitive.newPrimitive(arg));
		}

		String name = java.lang.System.getProperty("astra.name", "main");
		try {
			astra.core.Agent agent = new Main().newInstance(name);
			agent.initialize(new Goal(new Predicate("main", new Term[] { argList })));
			Scheduler.schedule(agent);
		} catch (AgentCreationException e) {
			e.printStackTrace();
		} catch (ASTRAClassNotFoundException e) {
			e.printStackTrace();
		};
	}
}
