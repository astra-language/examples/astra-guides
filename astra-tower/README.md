# astra-tower
This project is an example of an environment implemented purely in ASTRA as a 
custom module.

The environment is a classic tower world that consists of a table, four blocks
(a, b, c, d), and a gripper.  The agent can perform two basic actions: pickup(X)
and putdown(X, Y).  The example program constructs a tower c->b->a (with a at
the top).

# Running the example
Download the repository and run using:

`mvn compile astra:deploy`

