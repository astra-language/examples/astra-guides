/**
 * GENERATED CODE - DO NOT CHANGE
 */

import astra.core.*;
import astra.execution.*;
import astra.event.*;
import astra.messaging.*;
import astra.formula.*;
import astra.lang.*;
import astra.statement.*;
import astra.term.*;
import astra.type.*;
import astra.tr.*;
import astra.reasoner.util.*;


public class Main extends ASTRAClass {
	public Main() {
		setParents(new Class[] {astra.lang.Agent.class});
		addInference(new Inference(
			new Predicate("free", new Term[] {
				new Variable(Type.STRING, "X",false)
			}),
			new NOT(
				new Predicate("on", new Term[] {
					new Variable(Type.STRING, "Y",false),
					new Variable(Type.STRING, "X")
				})
			)
		));
		addInference(new Inference(
			new Predicate("free", new Term[] {
				new Variable(Type.STRING, "X",false)
			}),
			new Predicate("table", new Term[] {
				new Variable(Type.STRING, "X")
			})
		));
		addRule(new Rule(
			"Main", new int[] {16,9,16,28},
			new GoalEvent('+',
				new Goal(
					new Predicate("main", new Term[] {
						new Variable(Type.LIST, "args",false)
					})
				)
			),
			Predicate.TRUE,
			new Block(
				"Main", new int[] {16,27,19,5},
				new Statement[] {
					new ModuleCall("tower",
						"Main", new int[] {17,8,17,20},
						new Predicate("init", new Term[] {}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((Tower) intention.getModule("Main","tower")).init(
								);
							}
						}
					),
					new Subgoal(
						"Main", new int[] {18,8,19,5},
						new Goal(
							new Predicate("tower", new Term[] {
								Primitive.newPrimitive("a"),
								Primitive.newPrimitive("b"),
								Primitive.newPrimitive("c")
							})
						)
					)
				}
			)
		));
		addRule(new Rule(
			"Main", new int[] {21,9,21,66},
			new GoalEvent('+',
				new Goal(
					new Predicate("tower", new Term[] {
						new Variable(Type.STRING, "X",false),
						new Variable(Type.STRING, "Y",false),
						new Variable(Type.STRING, "Z",false)
					})
				)
			),
			new Predicate("table", new Term[] {
				new Variable(Type.STRING, "T",false)
			}),
			new Block(
				"Main", new int[] {21,65,25,5},
				new Statement[] {
					new Subgoal(
						"Main", new int[] {22,8,25,5},
						new Goal(
							new Predicate("holding", new Term[] {
								new Variable(Type.STRING, "Z")
							})
						)
					),
					new Subgoal(
						"Main", new int[] {22,21,25,5},
						new Goal(
							new Predicate("on", new Term[] {
								new Variable(Type.STRING, "Z"),
								new Variable(Type.STRING, "T")
							})
						)
					),
					new Subgoal(
						"Main", new int[] {23,8,25,5},
						new Goal(
							new Predicate("holding", new Term[] {
								new Variable(Type.STRING, "Y")
							})
						)
					),
					new Subgoal(
						"Main", new int[] {23,21,25,5},
						new Goal(
							new Predicate("on", new Term[] {
								new Variable(Type.STRING, "Y"),
								new Variable(Type.STRING, "Z")
							})
						)
					),
					new Subgoal(
						"Main", new int[] {24,8,25,5},
						new Goal(
							new Predicate("holding", new Term[] {
								new Variable(Type.STRING, "X")
							})
						)
					),
					new Subgoal(
						"Main", new int[] {24,21,25,5},
						new Goal(
							new Predicate("on", new Term[] {
								new Variable(Type.STRING, "X"),
								new Variable(Type.STRING, "Y")
							})
						)
					)
				}
			)
		));
		addRule(new Rule(
			"Main", new int[] {27,9,27,61},
			new GoalEvent('+',
				new Goal(
					new Predicate("holding", new Term[] {
						new Variable(Type.STRING, "X",false)
					})
				)
			),
			new AND(
				new NOT(
					new Predicate("holding", new Term[] {
						new Variable(Type.STRING, "Y",false)
					})
				),
				new Predicate("free", new Term[] {
					new Variable(Type.STRING, "X")
				})
			),
			new Block(
				"Main", new int[] {27,60,30,5},
				new Statement[] {
					new ModuleCall("console",
						"Main", new int[] {28,8,28,43},
						new Predicate("println", new Term[] {
							Operator.newOperator('+',
								Primitive.newPrimitive("picking up: "),
								new Variable(Type.STRING, "X")
							)
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Console) intention.getModule("Main","console")).println(
									(java.lang.String) intention.evaluate(predicate.getTerm(0))
								);
							}
						}
					),
					new ModuleCall("tower",
						"Main", new int[] {29,8,29,23},
						new Predicate("pickup", new Term[] {
							new Variable(Type.STRING, "X")
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((Tower) intention.getModule("Main","tower")).pickup(
									(java.lang.String) intention.evaluate(predicate.getTerm(0))
								);
							}
						}
					)
				}
			)
		));
		addRule(new Rule(
			"Main", new int[] {32,9,32,58},
			new GoalEvent('+',
				new Goal(
					new Predicate("on", new Term[] {
						new Variable(Type.STRING, "X",false),
						new Variable(Type.STRING, "Y",false)
					})
				)
			),
			new AND(
				new Predicate("holding", new Term[] {
					new Variable(Type.STRING, "X")
				}),
				new Predicate("free", new Term[] {
					new Variable(Type.STRING, "Y")
				})
			),
			new Block(
				"Main", new int[] {32,57,35,5},
				new Statement[] {
					new ModuleCall("console",
						"Main", new int[] {33,8,33,53},
						new Predicate("println", new Term[] {
							Operator.newOperator('+',
								Primitive.newPrimitive("putting: "),
								Operator.newOperator('+',
									new Variable(Type.STRING, "X"),
									Operator.newOperator('+',
										Primitive.newPrimitive(" on: "),
										new Variable(Type.STRING, "Y")
									)
								)
							)
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Console) intention.getModule("Main","console")).println(
									(java.lang.String) intention.evaluate(predicate.getTerm(0))
								);
							}
						}
					),
					new ModuleCall("tower",
						"Main", new int[] {34,8,34,27},
						new Predicate("putdown", new Term[] {
							new Variable(Type.STRING, "X"),
							new Variable(Type.STRING, "Y")
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((Tower) intention.getModule("Main","tower")).putdown(
									(java.lang.String) intention.evaluate(predicate.getTerm(0)),
									(java.lang.String) intention.evaluate(predicate.getTerm(1))
								);
							}
						}
					)
				}
			)
		));
	}

	public void initialize(astra.core.Agent agent) {
	}

	public Fragment createFragment(astra.core.Agent agent) throws ASTRAClassNotFoundException {
		Fragment fragment = new Fragment(this);
		fragment.addModule("console",astra.lang.Console.class,agent);
		fragment.addModule("tower",Tower.class,agent);
		return fragment;
	}

	public static void main(String[] args) {
		Scheduler.setStrategy(new AdaptiveSchedulerStrategy());
		ListTerm argList = new ListTerm();
		for (String arg: args) {
			argList.add(Primitive.newPrimitive(arg));
		}

		String name = java.lang.System.getProperty("astra.name", "main");
		try {
			astra.core.Agent agent = new Main().newInstance(name);
			agent.initialize(new Goal(new Predicate("main", new Term[] { argList })));
			Scheduler.schedule(agent);
		} catch (AgentCreationException e) {
			e.printStackTrace();
		} catch (ASTRAClassNotFoundException e) {
			e.printStackTrace();
		};
	}
}
