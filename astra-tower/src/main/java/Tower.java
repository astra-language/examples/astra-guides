import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import astra.core.Module;
import astra.formula.Predicate;
import astra.term.Primitive;
import astra.term.Term;

public class Tower extends Module {
    private String holding;
    private String table;
    private Set<String> block = new HashSet<String>();
    private Map<String, String> on = new HashMap<String, String>();
    
    // Beliefs
    Predicate holdingBelief;
    Map<String, Predicate> onBeliefs = new HashMap<String, Predicate>();

    @ACTION
    public boolean init() {
        table = "table";
        agent.beliefs().addBelief(new Predicate("table", new Term[] {Primitive.newPrimitive(table)}));

        Predicate onBelief = null;
        for (String b : new String[] {"a", "b","c","d"}) {
            block.add(b);
            
            agent.beliefs().addBelief(new Predicate("block", new Term[] {Primitive.newPrimitive(b)}));
            agent.beliefs().addBelief(onBelief = new Predicate("on", new Term[] {Primitive.newPrimitive(b), Primitive.newPrimitive(table)}));
            onBeliefs.put(b, onBelief);
        }
        return true;
    }

    @ACTION
    public boolean pickup(String X) {
        if (!isEmpty()) return false;
        
        holding = X;
        agent.beliefs().addBelief(holdingBelief = new Predicate("holding", new Term[] {Primitive.newPrimitive(holding)}));

        on.remove(X);
        agent.beliefs().dropBelief(onBeliefs.remove(X));

        return true;
    }

    @ACTION
    public boolean putdown(String X, String Y) {
        if (!isHolding(X) && !free(Y)) {
            System.out.println("Not holding: " + X);
            System.out.println("Not free: " + X);
            return false;
        }
        holding = null;
        agent.beliefs().dropBelief(holdingBelief);
        holdingBelief = null;

        Predicate onBelief = new Predicate("on", new Term[] {Primitive.newPrimitive(X), Primitive.newPrimitive(Y)});
        agent.beliefs().addBelief(onBelief);
        onBeliefs.put(X, onBelief);

        return true;
    }

    private boolean isHolding(String X) {
        return X.equals(holding);
    }

    private boolean isEmpty() {
        return holding == null;
    }

    private boolean free(String X) {
        if (X.equals(table)) return true;
        for (Entry<String, String> entry : on.entrySet()) {
            if (entry.getValue().equals(X)) return false;
        }
        return true;
    }
}