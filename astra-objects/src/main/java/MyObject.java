import java.util.Set;
import java.util.HashSet;

public class MyObject {
    public MyObject() {
        text = "I'm alive!";
        set.add("one");
        set.add("two");
    }

    public String text;
    public Set<String> set = new HashSet<>();

    public void mutate() {
        text = "I'm changing...";
    }
}
