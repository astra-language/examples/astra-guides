import astra.core.Module;
import astra.event.Event;
import astra.reasoner.Unifier;
import astra.term.Primitive;
import astra.term.Term;

public class HelloModule extends Module {
    static {
        Unifier.eventFactory.put(HelloEvent.class, new HelloEventUnifier());
    }
    
    @ACTION
    public boolean generate(String name) {
        agent.addEvent(new HelloEvent(Primitive.newPrimitive(name)));
        return true;
    }

    @EVENT( symbols={}, types = {"string"}, signature = "$hel")
    public Event hello(Term name) {
        return new HelloEvent(name);
    }
}