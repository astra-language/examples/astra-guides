import java.util.HashMap;
import java.util.Map;

import astra.core.Agent;
import astra.reasoner.EventUnifier;
import astra.reasoner.Unifier;
import astra.term.Term;

public class HelloEventUnifier implements EventUnifier<HelloEvent> {
	public Map<Integer, Term> unify(HelloEvent source, HelloEvent target, Agent agent) {
		return Unifier.unify(new Term[] {source.name() }, new Term[] {target.name() }, new HashMap<Integer, Term>(), agent);
	}
}
