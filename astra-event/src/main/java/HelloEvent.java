import astra.event.Event;
import astra.reasoner.util.LogicVisitor;
import astra.term.Term;

public class HelloEvent implements Event {
    private Term name;

    public HelloEvent(Term name) {
        this.name = name;
    }

    public Object getSource() {
        return null;
    }

    public String signature() {
        return "$hel";
    }

    public Event accept(LogicVisitor visitor) {
        return new HelloEvent((Term) name.accept(visitor));
    }

    public Term name() {
        return name;
    }
}