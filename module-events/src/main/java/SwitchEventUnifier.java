import java.util.HashMap;
import java.util.Map;

import astra.core.Agent;
import astra.reasoner.EventUnifier;
import astra.reasoner.Unifier;
import astra.term.Term;

public class SwitchEventUnifier implements EventUnifier<SwitchEvent> {
	public Map<Integer, Term> unify(SwitchEvent source, SwitchEvent target, Agent agent) {
		return Unifier.unify(new Term[] {source.state() }, new Term[] {target.state() }, new HashMap<Integer, Term>(), agent);
	}
}
