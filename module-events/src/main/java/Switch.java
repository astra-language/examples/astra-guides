import astra.core.Module;
import astra.event.Event;
import astra.reasoner.Unifier;
import astra.term.Primitive;
import astra.term.Term;

public class Switch extends Module {
  static {
    Unifier.eventFactory.put(SwitchEvent.class, new SwitchEventUnifier());
  }

  private boolean on = false;

  @ACTION
  public boolean flip() {
    on = !on;
    agent.addEvent(new SwitchEvent(Primitive.newPrimitive(on ? "on" : "off")));
    return true;
  }

  @EVENT(symbols = {}, types = { "string" }, signature = "$swe;")
  public Event event(Term state) {
    return new SwitchEvent(state);
  }
}