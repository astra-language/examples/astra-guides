import astra.event.Event;
import astra.reasoner.util.LogicVisitor;
import astra.term.Term;

public class SwitchEvent implements Event {
    private Term state;

    public SwitchEvent(Term state) {
        this.state = state;
    }
    
    @Override
    public Event accept(LogicVisitor visitor) {
        return new SwitchEvent(
            (Term) visitor.visit(state)
        );
    }

    @Override
    public Object getSource() {
        return null;
    }

    @Override
    public String signature() {
        return "$swe;";
    }

    public Term state() {
        return state;
    }
}